use libc::c_int;
use types::{HMODULE, LPCWSTR, LPCSTR, DWORD, BOOL, LARGE_INTEGER};

pub type FARPROC = unsafe extern "stdcall" fn () -> c_int;

#[link(name = "kernel32")]
extern "stdcall" {
	pub fn QueryPerformanceFrequency(lpFrequency: *mut LARGE_INTEGER) -> BOOL;

	pub fn QueryPerformanceCounter(lpPerformanceCount: *mut LARGE_INTEGER) -> BOOL;

    pub fn GetModuleHandleW(lpModuleName: LPCWSTR) -> HMODULE;

    pub fn GetLastError() -> DWORD;

    pub fn LoadLibraryW(lpFileName: LPCWSTR) -> HMODULE;

    pub fn FreeLibrary(hModule: HMODULE) -> BOOL;

    pub fn GetProcAddress(hModule: HMODULE, lpProcName: LPCSTR) -> FARPROC;
}