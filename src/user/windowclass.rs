#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use types::{UINT, HINSTANCE, HICON, HCURSOR, HBRUSH, LPCWSTR, ATOM, BOOL};
use user::events::WNDPROC;
use libc::c_int;

#[repr(C)]
pub struct WNDCLASSEXW {
    pub cbSize: UINT,
    pub style: UINT,
    pub lpfnWndProc: WNDPROC,
    pub cbClsExtra: c_int,
    pub cbWndExtra: c_int,
    pub hInstance: HINSTANCE,
    pub hIcon: HICON,
    pub hCursor: HCURSOR,
    pub hbrBackground: HBRUSH,
    pub lpszMenuName: LPCWSTR,
    pub lpszClassName: LPCWSTR,
    pub hIconSm: HICON,
}
impl Copy for WNDCLASSEXW {}

pub mod CS {
    use types::DWORD;

    pub const BYTEALIGNCLIENT: DWORD = 0x1000;
    pub const BYTEALIGNWINDOW: DWORD = 0x2000;
    pub const CLASSDC: DWORD = 0x0040;
    pub const DBLCLKS: DWORD = 0x0008;
    pub const DROPSHADOW: DWORD = 0x00020000;
    pub const GLOBALCLASS: DWORD = 0x4000;
    pub const HREDRAW: DWORD = 0x0002;
    pub const NOCLOSE: DWORD = 0x0200;
    pub const OWNDC: DWORD = 0x0020;
    pub const PARENTDC: DWORD = 0x0080;
    pub const SAVEBITS: DWORD = 0x0800;
    pub const VREDRAW: DWORD = 0x0001;
}

#[link(name = "user32")]
extern "stdcall" {
    pub fn RegisterClassExW(lpwcx: *const WNDCLASSEXW) -> ATOM;
    pub fn UnregisterClassW(lpClassName: LPCWSTR, hInstance: HINSTANCE) -> BOOL;
}